import time
def get_current_date():
	t = time.localtime()
	return '{}{}{}'.format(t.tm_year, _fill_zero(t.tm_month), _fill_zero(t.tm_mday))

def _fill_zero(num):
	return ''.format('0', str(num)) if num < 10 else num;