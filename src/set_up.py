from setuptools import setup, find_packages

import crawler

setup(
    name='crawler',
    version=crawler.__version__,
    description='Crawling tool',
    long_description='''
        This package provides functionbility to crawl stock information.
    ''',

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Intended Audience :: Developers',
    ],
    url=crawler.__url__,
    author=crawler.__author__,
    author_email=crawler.__author_email__,
    license=crawler.__license__,
    packages=find_packages(),
    #install_requires=[
    #    'MultipartPostHandler'
    #],
    zip_safe=False)