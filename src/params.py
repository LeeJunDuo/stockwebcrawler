# -*- coding: utf-8 -*-


def gen_all_stock_params(response = 'json', date = Date()):
	return {
	  'response': response,
	  'date': date,
	  'type': 'ALL'
	}

def gen_one_stock_params(response = 'json', date = Date(), stock_no):
	return {
	  'response': response,
	  'date': date,
	  'stockNo': stock_no
	}
