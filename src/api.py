# -*- coding: utf-8 -*-

import requests
import pandas as pd
import numpy as np
from io import StringIO

from .config import TARGET_HOST, TARGET_SEARCH_ALL_PATH, TARGET_SEARCH_ONE_STOCK_PATH
from .params import gen_one_stock_params
from ._internal_utils import get_current_date

def query_all():
    current_date = get_current_date()
    request_params = gen_all_stock_params('csv', current_date)

    str_list = []
    for i in r.text.split('\n'):
      if len(i.split('",')) == 17 and i[0] != '=':
        i = i.strip(",\r\n")
        str_list.append(i)

    df = pd.read_csv(StringIO("\n".join(str_list)))
    pd.set_option('display.max_rows', None)
    df.head(10)


    return requests.post(TARGET_HOST + TARGET_SEARCH_ALL_PATH, params = request_params)


def query_one(stock_no):
    current_date = get_current_date()
    request_params = gen_one_stock_params('json', current_date, stock_no)
    return requests.post(TARGET_HOST + TARGET_SEARCH_ALL_PATH, params = request_params)

